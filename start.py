__author__ = 'manfred'

#
# Citation Flight Log Software [Assignment 4]
# May 2014
#
# #    __  __ ____  _  _________  _______
# #   /  |/  / _ | / |/ / __/ _ \/ __/ _ \ Manfred
# #  / /|_/ / __ |/    / _// , _/ _// // / Josefsson
# # /_/  /_/_/ |_/_/|_/_/ /_/|_/___/____/  4269020
# #
#
# Designed for Python 2.7
#
# This software is released under the MIT license.

# Import Statements
from math import *
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import osmapi

# Initalie Variables
x = list()
y = list()
z = list()

points = np.genfromtxt("data_points.txt", dtype=None, skip_header=4)

for i in range(0, len(points)):
    x.append(points[i][1])
    y.append(points[i][2])
    z.append(points[i][3])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
mpl.rcParams['legend.fontsize'] = 10
ax = fig.gca(projection='3d')
theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)

ax.plot(x, y, z, label='Flight Path')
ax.plot(x, y, 0, label='Ground Track')

ax.legend()

plt.show()



# from pylab import *
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.cbook import get_sample_data
# from matplotlib._png import read_png
# fn = get_sample_data("lena.png", asfileobj=False)
# img = read_png(fn)
# x, y = ogrid[0:img.shape[0], 0:img.shape[1]]
# ax = gca(projection='3d')
# ax.plot_surface(x, y, 10, rstride=5, cstride=5, facecolors=img)
# show()
